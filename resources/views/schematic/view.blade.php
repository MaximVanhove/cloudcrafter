@extends('app')

@section('styles')
<link href="{{ asset('/css/view.css') }}" rel="stylesheet">
@endsection

@section('side-nav')
<ul>
    <li class="active"><a href>Files</a></li>
    <li><a href>Stats</a></li>
    <li><a href>Settings</a></li>
</ul>
@endsection

@section('content')
<a href=" {{action('SchematicController@get', $id)}} ">Download <?=$id?></a>
<a href=" {{action('AccountController@addToDrive', $id)}} ">Add to drive</a>
<div id="<?=$id?>" class="file">
    <div class="progress"></div>
    <img class="loading" src="{{ asset('/img/loading.gif') }}" />
    <div class="schematic-wrapper">
        <div class="schematic-container"></div>
    </div>
</div>

<div id="canvas"></div>
@endsection
