@extends('app')

@section('styles')
<link href="{{ asset('/css/server.css') }}" rel="stylesheet">
@endsection

@section('side-nav')
<ul>
    <li class="active"><a href>Files</a></li>
    <li><a href>Stats</a></li>
    <li><a href>Settings</a></li>
</ul>
@endsection

@section('content')
@for ($i = 0; $i < 10; $i++)
<div class="file">
    <div class="content">
        <div class="head">
            <img src="https://minespy.net/api/head/Notch/36" alt="head" />
        </div>
        <div class="date"><span>19 jun 2015</span></div>
        <a href class="btn view">View</a><a href class="btn download">Download</a>
    </div>
</div>
@endfor
@endsection

@section('sidebar')
<div class="widget"><div class="content"><h5>Storage</h5></div></div>
@endsection