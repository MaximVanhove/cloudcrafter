@extends('app')

@section('styles')
<link href="{{ asset('/css/account.css') }}" rel="stylesheet">
@endsection

@section('side-nav')
<ul>
    <li class="active"><a href>Files</a></li>
    <li><a href>Stats</a></li>
    <li><a href>Settings</a></li>
</ul>
@endsection

@section('content')

<div class="row">
    <h1>Today</h1>

    <div id="oMJC4553" class="file">
        <div class="progress"></div>
        <img class="loading" src="{{ asset('/img/loading.gif') }}" />
        <div class="schematic-wrapper">
            <div class="schematic-container"></div>
        </div>
    </div>
</div>

<div class="row">
    <h1>Last week</h1>

    <div id="Y11AQV6Y" class="file">
        <div class="progress"></div>
        <img class="loading" src="{{ asset('/img/loading.gif') }}" />
        <div class="schematic-wrapper">
            <div class="schematic-container"></div>
        </div>
    </div>

    <div id="2Jy32K41" class="file">
        <div class="progress"></div>
        <img class="loading" src="{{ asset('/img/loading.gif') }}" />
        <div class="schematic-wrapper">
            <div class="schematic-container"></div>
        </div>
    </div>
</div>

<div id="canvas"></div>
@endsection

@section('sidebar')
<div class="widget"><div class="content"><h5>Storage</h5></div></div>
@endsection