<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CloudCrafter - app</title>
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    @yield('styles')
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="app-wrapper">

    <nav id="app-nav">
        <a href data-target="menu" class="left"><img src="{{ asset('/icons/menu.svg') }}" alt=""></a>
        <a href class="right"><img src="{{ asset('/icons/profile.svg') }}" alt=""></a>
        <div class="middle"><a href><img src="{{ asset('/icons/logo.svg') }}" alt=""></a></div>
    </nav>

    <nav id="app-side-nav">@yield('side-nav')</nav>
    <div id="app-content-wrapper">
        <div id="content-wrapper">@yield('content')</div>
        <div id="sidebar-wrapper">@yield('sidebar')@include('partials.widgets.partners')</div>
    </div>
</div>

<!-- Scripts -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<!-- App -->
<script src="{{ asset('/js/all.js') }}"></script>
</body>
</html>
