@extends('app')

@section('styles')
<link href="{{ asset('/css/dashboard.css') }}" rel="stylesheet">
@endsection

@section('side-nav')
<ul>
    <li class="active"><a href>Dashboard</a></li>
    <li><a href>Servers</a></li>
    <li><a href>Settings</a></li>
</ul>
@endsection

@section('content')

<div class="knowledgebase">
<div class="content">
<div class="questions"><h3>Knowledge base</h3></div>
<div class="answer"><h3>Answer</h3></div>
</div>
</div>

<h1>Servers</h1>
@for ($i = 0; $i < 10; $i++)
<div class="server">
<a href>
    <div class="content">
        <h3>BlazeCraft Plots</h3>
        <p class="online">online</p>
        <p class="count">13 / 40 players</p>
    </div>
</a>
</div>
@endfor
<div class="server">
<a href>
    <div class="content">
        <h1>+ Add a server</h1>
    </div>
</a>
</div>
@endsection

@section('sidebar')
<div class="widget"><div class="content"></div></div>
@endsection