/**
 * Created by MaximVanhove on 21/06/15.
 */

$().ready(function(){

    $('#app-content-wrapper').click(function(){
        $('#app-side-nav').removeClass('open');
    });

    $('nav a').click(function(e){
        e.preventDefault();
        var $target = $(this).attr('data-target');
        if($target == "menu"){
            $('#app-side-nav').toggleClass('open');
        }
    });

    $(window).resize(function(){
        $('#app-side-nav').removeClass('open');
    });

    var files = [];

    $('.file').each( function() {
        if(this.id){
            console.log(this.id + ": " + localStorage.getItem(this.id));
            if(false && localStorage.getItem(this.id) != null && typeof(Storage) !== "undefined"){
                $('#'+this.id+' .schematic-container').css('background-image', 'url(\'' + localStorage.getItem(this.id) + '\')');
                $('#'+this.id).addClass('loaded');
            }
            else files.push(this.id);
        }
    });

    function renderOneAtTheTime(schematicID){
        if(files.length > 0 || schematicID != null) {
            if (schematicID == null) {
                schematicID = files.shift();
            }
            console.log("drawing: " + schematicID);
            var schematicAPI = "/preview/" + schematicID;
            $.getJSON(schematicAPI)
                .done(function (data) {
                    drawSchematic(data, schematicID);
                });
        }
    }
    renderOneAtTheTime();

    function drawSchematic(schematic, schematicID) {
        var blocks = new Array();
        var blocksToload = 0;
        var iconWidth = 66;
        var iconHeight = 74;
        var schematicX = schematic.length;
        var schematicY = schematic[0].length;
        var schematicZ = schematic[0][0].length;

        // Calculate width and height of canvas*/
        var height = (iconHeight + (schematicY - 1) * (iconHeight / 2)) + (iconWidth / 4) * (schematicX + schematicZ - 2);
        var width = (schematic.length + schematic[0][0].length - 2) * (iconWidth / 2) + iconWidth;
        var canvas = document.createElement("canvas");

        var factor = 1 / (width / 800);

        canvas.setAttribute("width", width * factor + "px");
        canvas.setAttribute("height", height * factor + "px");
        document.getElementById("canvas").appendChild(canvas);

        var context = canvas.getContext("2d");
        var img = canvas.toDataURL("image/png");
        var coords = [];

        context.scale(factor, factor);

        var $progress = $('#'+schematicID + ' .progress');

        // Creating a printer which will print the image on the right place*/
        var printer = function (x, y) {
            this.cx = x;
            this.cy = y;
            this.x = 0;
            this.y = 0;
            this.z = 0;
            this.factor = 1;
            this.draw = function (id) {
                var drawY = this.cy;
                var drawX = this.cx;
                drawY -= this.y * (iconHeight / 2) / this.factor;
                drawX -= this.x * (iconWidth / 2) / this.factor;
                drawY += this.x * (iconWidth / 4) / this.factor;
                drawY += this.z * (iconWidth / 4) / this.factor;
                drawX += this.z * (iconWidth / 2) / this.factor;

                context.drawImage(blocks[id], 0, 0, 66, 74, drawX, drawY, 66 / this.factor, 74 / this.factor);
            }
        };

        // Getting all block images we need, so we don't load unneeded blocks*/
        for (var x in schematic) {
            for (var y in schematic[x]) {
                for (var z in schematic[x][y]) {
                    coords.push({x: x, y: y, z: z})
                    if (blocks[schematic[x][y][z]] == null) {
                        blocks[schematic[x][y][z]] = "/img/blocks/" + schematic[x][y][z] + ".png";
                        blocksToload++;
                    }
                }
            }
        }

        // Load images
        blocks.forEach(function (value, index) {
            var path = blocks[index];
            var img = new Image();
            img.src = path;
            img.onerror = function () {
                img.src = "/img/blocks/0.png";
                console.log("replacing " + path + " with air");
            };
            img.onload = function () {
                blocks[index] = img;
                blocksToload--;
                if (blocksToload == 0) {
                    process();
                }
            };
        });

        // Y11AQV6Y = 27
        // oMJC4553 = 104346
        // 2Jy32K41 = million

        var t0 = performance.now();
        var p = new printer((iconWidth / 2) * (schematic.length - 1), 0 + ((schematicY - 1) * (iconHeight / 2)));
        var process_index = 0;
        var process_delay = 1;
        var process_chunk = 0;
        var process = function () {
            p.x = coords[process_index].x;
            p.y = coords[process_index].y;
            p.z = coords[process_index].z;
            p.draw(schematic[coords[process_index].x][coords[process_index].y][coords[process_index].z]);

            process_index++;
            process_chunk++;
            if (process_index < coords.length) {
                if (process_chunk > 3000) {
                    setTimeout(process, process_delay);
                    process_chunk = 0;
                    $progress.css('width', Math.ceil(process_index/coords.length*100)+"%");
                }
                else {
                    process();
                }
            }
            else {
                //End of process
                // Create an image of the canvas to have a responsive view
                var img = canvas.toDataURL("image/png");
                $('#'+schematicID+' .schematic-container').css('background-image', 'url(\'' + img + '\')');
                $('#'+schematicID).addClass('loaded');
                if(typeof(Storage) !== "undefined") localStorage.setItem(schematicID, img);
                console.log("localStorage");
                var t1 = performance.now();
                console.log("Rendering "+schematicID+" took " + (t1 - t0) + " milliseconds.");
                document.getElementById("canvas").innerHTML = "";
                $progress.remove();
                setTimeout(renderOneAtTheTime, 1000);
            }
            ;
        }
    }
});
