<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchematicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schematics', function(Blueprint $table)
        {
            $table->increments('id');
            $table->char('schematic_id', 16);
            $table->string('filename');
            $table->string('mime');
            $table->timestamps();
        });

        Schema::create('schematic_user', function(Blueprint $table)
        {
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('schematic_id')->unsigned()->index();
            $table->foreign('schematic_id')->references('id')->on('schematics')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('schematic_user');
        Schema::drop('schematics');
    }
}
