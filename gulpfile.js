var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.sourcemaps = false;

elixir(function(mix) {
    mix.sass(["app.scss"]);
    mix.sass("server.scss", 'public/css/server.css');
    mix.sass("account.scss", 'public/css/account.css');
    mix.sass("view.scss", 'public/css/view.css');
    mix.sass("dashboard.scss", 'public/css/dashboard.css');
    mix.scripts(["app.js"]);
});