<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    protected $fillable = [
        'name',
        'body',
        'published_at'
    ];

    public function user(){
        return $this->belongsTo('App/User');
    }
}
