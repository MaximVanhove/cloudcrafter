<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schematic extends Model
{
    protected $fillable = [ ];

    /**
    * Get the users who added this schematic to their drive.
    *
    * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
    */
    public function users(){
        return $this->belongsToMany('App\User');
    }
}
