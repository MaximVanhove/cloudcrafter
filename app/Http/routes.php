<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('home', function () {
    return view('welcome');
});

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Route::get('servers/{server}', 'ServersController@index');
Route::get('dashboard', 'DashboardController@index');
Route::get('drive', 'AccountController@index');
Route::get('user', 'AccountController@drive');
Route::get('addToDrive/{id}', 'AccountController@addToDrive');
Route::get('view/{id}', 'SchematicController@show');

// API
Route::get('preview/{id}', 'SchematicController@json');

// File upload & download
Route::get('fileentry', 'SchematicController@index');
Route::get('file/get/{id}', [
    'as' => 'getentry', 'uses' => 'SchematicController@get']);
Route::get('addme/{id}/{filename}', 'SchematicController@addme');
