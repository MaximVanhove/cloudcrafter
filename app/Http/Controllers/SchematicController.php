<?php

namespace App\Http\Controllers;

use App;
use App\Schematic;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class SchematicController extends Controller
{
    public function index()
    {
        $schematics = Schematic::all();

        return view('schematic.index', compact('schematics'));
    }

    public function show($id)
    {
        return view('schematic.view', compact('id'));
    }

    public function json($id)
    {
        if (Storage::disk('schematics')->exists($id.'.zip'))
        {
            $contents = Storage::disk('schematics')->get($id.'.zip');
            //$contents = Storage::get($id.'.zip');
            $file = fopen($id.'.zip', "w");
            fwrite($file, $contents);
            fclose($file);

            $json = file_get_contents('zip://'.$id.'.zip#'.$id.'.json');
            $json = trim(preg_replace('/\s+/', ' ', $json));
            unlink($id.'.zip');

            return $json;
        }
        return null;
    }

    public function get($id){

        $entry = Schematic::where('schematic_id', '=', $id)->firstOrFail();
        $file = Storage::disk('schematics')->get($entry->filename);

        $file = Storage::disk('schematics')->get($id.'.zip');
        //$file = Storage::disk('schematics')->get($id.'.schematic');

        return (new Response($file, 200))
            ->header('Content-Type', "text/plain");
    }

    public function addme($id, $filename){

        $pieces = explode(".", $filename);
        $extensie = $pieces[1];

        if($extensie == 'zip' || $extensie == 'schematic'){
            $file = App::make('url')->to('/') . "/schematics/".$filename;

            Storage::disk('schematics')->put($filename,  file_get_contents($file));

            if($extensie == 'schematic'){
                $entry = new Schematic();
                $entry->mime = "text/plain";
                $entry->filename = $filename;
                $entry->schematic_id = $id;

                $entry->save();
                return "Schematic object created ;)";
            }

            return "File stored";
        }

        else return "Wrong extension: ".$extensie;
    }
}
